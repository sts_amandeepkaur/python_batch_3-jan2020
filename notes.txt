AJAX - Asynchronous JavaScript and XML (Extensible Markup Language)
    - Request/ Response to the server at backend (without releading the page)

Applications:
    - Chat System
    - Cart 
    - Live Matches
    - Sign Up Page

AJAX using JQuery:

$.ajax({
    url:"URL to request",
    type:"get/post",
    data:{"name":"Aman"},
    success:function(ab){
        alert(ab);
    }
})

DATA TABLE

1. Go to https://datatables.net/
2. Copy CSS link in your HTMl document <link>
3. Copy JS link in your HTMl document <script>
4. Write https: before link
5. Assign ID name to table.
6. Call .DataTable() function on table using id.

For e.g.

    $("#abcd").DataTable()